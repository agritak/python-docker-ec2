ARG PYTHON_VERSION=3.10.0
FROM python:${PYTHON_VERSION}

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

# Copy the source code into the container.
COPY . .

# Expose the port that the application listens on.
EXPOSE 9001

# Run the application.
CMD uvicorn 'main:app' --host=0.0.0.0 --port=9001
